//------------------------------------------------------------------------------
//
//   STAR PLATINA Copyright 2013 
//   Blandon Du All rights reserved. 
//
//------------------------------------------------------------------------------

package com.starplatina.game.puzzle.helper
{

	public class ArrayElementsSwapper
	{
		static public function swapInArray(arr:Object, fromIndex:int, toIndex:int):void
		{
			var temp:* = arr[toIndex];
			arr[toIndex] = arr[fromIndex];
			arr[fromIndex] = temp;
		}

		static public function swapBetweenArrays(a:Object, b:Object, hIndex:int):void
		{
			var temp:* = a[hIndex];
			a[hIndex] = b[hIndex];
			b[hIndex] = temp;
		}

		static public function swapInAndOutOfArray(a:Object, b:Object, arr:Object):void
		{
			var index:int = arr["indexOf"](a);
			arr[index] = b;
		}
	}
}
