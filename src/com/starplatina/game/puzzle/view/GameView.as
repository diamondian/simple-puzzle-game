//------------------------------------------------------------------------------
//
//   STAR PLATINA Copyright 2013 
//   Blandon Du All rights reserved. 
//
//------------------------------------------------------------------------------

package com.starplatina.game.puzzle.view
{
	import com.adobe.utils.ArrayUtil;
	import com.greensock.TweenLite;
	import com.starplatina.game.puzzle.enum.AssetNames;
	import com.starplatina.game.puzzle.view.element.Gem;
	
	import flash.geom.Point;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.AssetManager;

	public class GameView extends Sprite
	{
		static private const CANVAS_POSITION:Point = new Point(345, 120);
		static private const GRID_SIZE:Point = new Point(38, 38);
		static private const NUMBER_ROWS:int = 8;
		static private const NUMBER_COLUMNS:int = 8;


		private var _assetManager:AssetManager;
		private var _background:Image;
		private var _canvas:GridCanvas;
		
		private var _gems:Vector.<Gem>
		private var _grid:Vector.<Vector.<Gem>>;

		private var _gemsToClearHorizontally:Array;
		private var _gemsToClearVertically:Array;
		
		private var _clearingGems:Vector.<Gem>;
		private var _newGems:Vector.<Gem>;
		private var _fallingGems:Vector.<Gem>;
		//    flags
		private var _numberGemSwapped:int;
		private var _numberGemCleared:int;
		private var _numberGemLanded:int;
		private var _numberGemRefreshed:int;

		private var mouseController:MouseController;



		public function GameView(assetManager:AssetManager)
		{
			_assetManager = assetManager;
			
			initializeProperties();
			initializeController();
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			createChildren();
		}
		//==================================================================================== initialization ================
		private function initializeProperties():void
		{
			_gems = new Vector.<Gem>();
			_grid = new Vector.<Vector.<Gem>>();
			_fallingGems = new Vector.<Gem>();
			
			_gemsToClearHorizontally = [];
			_gemsToClearVertically = [];
			
			for (var y:int = 0; y < NUMBER_ROWS; y++) 
			{
				var row:Vector.<Gem> = new Vector.<Gem>();
				for (var x:int = 0; x < NUMBER_COLUMNS; x++) 
				{
					var gem:Gem = new Gem();
					gem.width = GRID_SIZE.x;
					gem.height = GRID_SIZE.y;
					gem.hIndex = x;
					gem.vIndex = y;
					
					gem.sigStateChanged.add(onGemStateChanged);
					gem.sigGemSwapped.add(onGemSwappingAnimationComplete);
					gem.sigGemCleared.add(onGemCleared);
					gem.sigStateLanded.add(onGemLanded);
					gem.sigGemRefreshed.add(onGemRefreshed);
					
					row.push(gem);
					_gems.push(gem);
				}
				_grid.push(row);
			}
			
			setUpRelationshipForGems();
		}
		
		private function initializeController():void
		{
			mouseController = new MouseController();
			mouseController.buildSensors(new Point(CANVAS_POSITION.x,CANVAS_POSITION.y),NUMBER_COLUMNS,NUMBER_ROWS,GRID_SIZE.x,GRID_SIZE.y);
			mouseController.sigSwap.add(onSwap);
			mouseController.sigSelected.add(onGemSelected);
		}
		
		private function createChildren():void
		{
			var w:Number = GRID_SIZE.x * NUMBER_ROWS;
			var h:Number =  GRID_SIZE.y * NUMBER_COLUMNS;
			
			_background = new Image(_assetManager.getTexture(AssetNames.BackGround));
			_canvas = new GridCanvas(_assetManager,w,h);
			_canvas.gems = _gems;
			
			_canvas.x = CANVAS_POSITION.x;
			_canvas.y = CANVAS_POSITION.y;
			
			var label:TextField = new TextField(300,25,"blandon.truegrave@gmail.com","Arial",15);
			var label1:TextField = new TextField(300,25,"www.starplatina.com","Arial",15);
			label.x = 280;
			label1.x = 280;
			label.y = 40;
			label1.y = 60;
			
			this.addChild(_background);
			this.addChild(_canvas);
			this.addChild(label);
			this.addChild(label1);
			
			getRandomNewGems();//---------------- get new gems to full fill grid.
			_canvas.updateGridView();
		}
		
		private function setUpRelationshipForGems():void
		{
			for each (var gem:Gem in _gems) 
			{
				gem.neighbors = [getGemByCoordinate(gem.hIndex-1,gem.vIndex),
					getGemByCoordinate(gem.hIndex+1,gem.vIndex),
					getGemByCoordinate(gem.hIndex,gem.vIndex-1),
					getGemByCoordinate(gem.hIndex,gem.vIndex+1)];
			}
		}
		
		//==================================================================================== signal callbacks ================
		
		private function onGemLanded(gem:Gem):void
		{
			_numberGemLanded ++;
			if(_numberGemLanded >= _fallingGems.length)
			{
				_numberGemLanded = 0;
				trace("all gems are landed");	
				refreshGems();
			}
		}
		
		private function refreshGems():void
		{
			getRandomNewGems();
			for each (var gem:Gem in _newGems) 
			{
				gem.playRefreshAnim();
			}
			for each (gem in _clearingGems) 
			{
				gem.scale = 0.9;
				//trace("scale back"+_clearingGems);
			}
		}
		
		private function onGemRefreshed(gem:Gem):void
		{
			_numberGemRefreshed ++;
			if(_numberGemRefreshed >= _newGems.length)
			{
				_numberGemRefreshed = 0;
				_canvas.updateGridView();//render an extra time.
				
				trace("all gems are refreshed----round complete-----------------------");
				_canvas.isActive = false;			
				mouseController.enabled = true;
			}
		}
		private function onGemCleared(gem:Gem):void
		{
			_numberGemCleared ++;
			if(_numberGemCleared >= _clearingGems.length)
			{
				_numberGemCleared = 0;
				_canvas.updateGridView();//render an extra time.
				
				trace("all gems are cleared");
				TweenLite.delayedCall(0.15,function():void
				{
					playDroppingGemAnimations();	//ensure all gems disapear before play falling animation.
					
					if(_fallingGems.length == 0) //when on the top,no gems are supposed to fall,refresh gems directly.
					{
						refreshGems();
					}
				});
			}
		}
		
		private function onGemSwappingAnimationComplete(gem:Gem):void
		{
			_numberGemSwapped ++
			//trace(_numberGemSwapped);
			if(_numberGemSwapped > 1)
			{
				_numberGemSwapped = 0;
				_canvas.updateGridView();//render an extra time.
				_canvas.isActive = false;
				
				if(_clearingGems.length > 0 )
				{
				    clearGems();//mouse will be enabled again after all new gems are set.
				}
				else
				{
					mouseController.enabled = true;
				}
			}
		}
		
		private function onGemStateChanged(gem:Gem):void
		{
			_canvas.updateGridView();
		}
		
		//==================================================================================== mouse interactions ================
		private function onGemSelected(gemCoordinate:Point):void
		{
			var gem:Gem = getGemByCoordinate(gemCoordinate.x,gemCoordinate.y);
			gem.selected = !gem.selected;
		}
		
		private function onSwap(coodinatesCodes:Array):void
		{
			mouseController.enabled = false;
			
			var c1:Point = coodinatesCodes[0] as Point;
			var c2:Point = coodinatesCodes[1] as Point;
			
			var gem1:Gem = getGemByCoordinate(c1.x,c1.y);
			var gem2:Gem = getGemByCoordinate(c2.x,c2.y);
			
			swapGemKind(gem1,gem2);
			var clearable:Boolean = checkResult(new <Gem>[gem1,gem2]);
			swapGemKind(gem1,gem2);
			
			_canvas.isActive = true;
			gem1.playSwapAnim(gem2,clearable);
		}
		
		private function swapGemKind(gem1:Gem,gem2:Gem):void
		{
			var type:int = gem1.gemType;
			gem1.gemType = gem2.gemType;
			gem2.gemType = type;
		}
	
		//==================================================================================== data manipulations ================
		private function getGemByCoordinate(x:int,y:int):Gem
		{
			if(x < 0 || y < 0 || x > NUMBER_COLUMNS - 1 || y > NUMBER_ROWS - 1)
			{
				return null;
			}
			return _grid[y][x];
		}

		private function getRandomNewGems(clearableResult:Boolean = false):void
		{
			_newGems ||= _gems.concat();
			
			var spectrum:int = AssetNames.GEM_NAMES.length;
			
			for each (var gem:Gem in _newGems) 
			{
				gem.gemType = Math.random() * spectrum;
			}
			
			clearableResult && !checkResult(_newGems) && getRandomNewGems(true);//only check result for new gems.
		}
		
		private function checkResult(checkingRange:Vector.<Gem>):Boolean
		{
			trace("Checking result....   -----------------------");
			_gemsToClearHorizontally = [];
			_gemsToClearVertically = [];
			
			checkingRange ||= _gems.concat();
			for each (var gem:Gem in checkingRange) 
			{
				if(gem.isFormingChainHorizontally && _gemsToClearHorizontally.indexOf(gem) == -1){
					_gemsToClearHorizontally = _gemsToClearHorizontally.concat(gem.horizontalChain);
				}
				if(gem.isFormingChainVertically && _gemsToClearVertically.indexOf(gem) == -1){
					_gemsToClearVertically = _gemsToClearVertically.concat(gem.verticalChain);
				}
			}
			_gemsToClearHorizontally = ArrayUtil.createUniqueCopy(_gemsToClearHorizontally);
			_gemsToClearVertically = ArrayUtil.createUniqueCopy(_gemsToClearVertically);
			_clearingGems = Vector.<Gem>(ArrayUtil.createUniqueCopy(_gemsToClearHorizontally.concat(_gemsToClearVertically)));
			//sort gems with bottom first order.
			_gemsToClearHorizontally.sortOn("hIndex");
			_gemsToClearVertically.sortOn("vIndex");
			trace("h result:"+_gemsToClearHorizontally);
			trace("v result:"+_gemsToClearVertically);
			
			trace("total gems can be cleared : "+_clearingGems.length);
			return _clearingGems.length > 0;
		}		
		
		private function clearGems():void
		{
			trace("clear gems!");
			_canvas.isActive = true;
			for each (var gem:Gem in _clearingGems) 
			{
				gem.clear();
			}
		}		
		
		private function playDroppingGemAnimations():void
		{
			//trace(_clearingGems);
			_fallingGems = new Vector.<Gem>();
			_newGems = new Vector.<Gem>();
			
			for each (var gem:Gem in _clearingGems) 
			{
				var hasClearingGemAbove:Boolean = _clearingGems.indexOf(gem.gemTop) != -1;
				var numberClearedGemsOnColumn:int = getNumberClearedGemsOnColumn(gem.hIndex);
				
				if(!hasClearingGemAbove){
					for (var i:int = 0; i < gem.vIndex; i++) 
					{
						var fallingGem:Gem = getGemByCoordinate(gem.hIndex,i);
						_fallingGems.push(fallingGem);
						playFallingGem(fallingGem,numberClearedGemsOnColumn);
					}
				}
				//as they are invisible at this moment, so change it now.
				gem.gemType = getGemTypeFrom(gem,gem.vIndex - numberClearedGemsOnColumn);
			}
			
			function getNumberClearedGemsOnColumn(columnIndex:int):int
			{
				var n:int;
				for each (var g:Gem in _clearingGems) 
				{
					if(g.hIndex == columnIndex){
						n++;
					}
				}
				return n;
			}
			
			function playFallingGem(gem:Gem,distance:int):void
			{
				var targetGem:Gem = getGemByCoordinate(gem.hIndex,gem.vIndex + distance);//get landing position
				var newKind:int = getGemTypeFrom(gem,gem.vIndex - distance);
				//trace("falling gems :"+gem+" new type:"+newKind+",drop to:"+targetGem);
				gem.playFallAnim(targetGem,	newKind);
			}
			
			//trace("new gems:"+_newGems);
			
		}
		
		private function getGemTypeFrom(targetGem:Gem,fromGemPosition:int):int
		{
			var fromGem:Gem = getGemByCoordinate(targetGem.hIndex,fromGemPosition)
			if(fromGem){
				return fromGem.gemType;
			}
			_newGems.push(targetGem);//searched position is null,we will get a new validated random gem type for it later(when all gems are landed)
			return fromGemPosition;//-1 will trigger the x,y to be decreased to minus(above the grid)
		}		
		
	}
}