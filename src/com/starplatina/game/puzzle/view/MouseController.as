//##############################################################################
//
//  Copyright 2013 Blandon.Du   All rights reserved.  
//
//##############################################################################

package com.starplatina.game.puzzle.view
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;

	public class MouseController
	{
		private var container:Sprite;

		private const _sigSwap:Signal = new Signal(Array);
		private const _sigSelected:Signal = new Signal(Point);
		
		private var _enabled:Boolean = true;

		private var sensorFirstClick:Sprite;

		public function MouseController()
		{
		}

		public function get enabled():Boolean
		{
			return _enabled;
		}

		public function set enabled(value:Boolean):void
		{
			_enabled = value;
		}

		public function get sigSelected():Signal
		{
			return _sigSelected;
		}

		public function get sigSwap():Signal
		{
			return _sigSwap;
		}

		public function buildSensors(initialPoint:Point, numberColumn:int, numberRow:int, width:int, height:int):void
		{
			container = Starling.current.nativeOverlay;


			for (var y:int = 0; y < numberRow; y++)
			{
				for (var x:int = 0; x < numberColumn; x++)
				{
					var sensor:Sprite = new Sprite();
					sensor.graphics.beginFill(0, 0.2);
					sensor.graphics.drawRect(0, 0, width - 2, height - 2);
					sensor.graphics.endFill();

					sensor.name = x + "" + y;
					sensor.x = initialPoint.x + x * width;
					sensor.y = initialPoint.y + y * height;

					sensor.addEventListener(MouseEvent.CLICK, onClick);
					container.addChild(sensor);
				}
			}
		}


		protected function onClick(event:MouseEvent):void
		{
			if(!_enabled)return;
			
			//---------------------------- 
			if (!sensorFirstClick)
			{
				sensorFirstClick = event.target as Sprite;				
				_sigSelected.dispatch(getCoordinate(sensorFirstClick));
			}
			else
			{
				if (sensorFirstClick != event.target)
				{
					var p1:Point = getCoordinate(sensorFirstClick);
					var p2:Point = getCoordinate(event.target as Sprite);
					
					if(isNearby(p1, p2))
					{
						_sigSelected.dispatch(getCoordinate(event.target as Sprite));;
						sensorFirstClick = null;
						
						_sigSwap.dispatch([p1, p2]);
					}
				}
				else
				{
					_sigSelected.dispatch(getCoordinate(sensorFirstClick));
					sensorFirstClick = null;
				}
			}
			
		}

		private function isNearby(p1:Point, p2:Point):Boolean
		{
			return ((Math.abs(p1.x - p2.x) == 1) && (p2.y == p1.y)) || ((Math.abs(p1.y - p2.y) == 1) && (p2.x == p1.x));
		}

		private function getCoordinate(sensor:Sprite):Point
		{
			return new Point(Number(sensor.name.substr(0, 1)), Number(sensor.name.substr(-1, 1)));
		}
	}
}
