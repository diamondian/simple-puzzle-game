//------------------------------------------------------------------------------
//
//Created by Blandon 
//
//------------------------------------------------------------------------------
package com.starplatina.game.puzzle.core
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.utils.AssetManager;

	public class StarlingInitializer
	{

		private var _onStarlingInitialized:Signal = new Signal(StarlingPortal);

		private var _starling:Starling;
		private var _stage:Stage;
		private var _assets:AssetManager;

		public function StarlingInitializer(main:Stage)
		{
			_stage = main;
		}

		public function initialize(viewPort:Rectangle, appWidth : int, appHeight : int, file:File):AssetManager
		{
			var iOS:Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;
			var isDebug:Boolean = Capabilities.isDebugger;
			
			_assets = new AssetManager();
			if(file){
				_assets.verbose = Capabilities.isDebugger;
				_assets.enqueue(file);
			}
			

			// launch Starling

			_starling = new Starling(StarlingPortal , _stage);
			_starling.viewPort = viewPort;
			_starling.stage.stageWidth = appWidth;
			_starling.stage.stageHeight = appHeight;
			_starling.simulateMultitouch = false;
			_starling.enableErrorChecking = Capabilities.isDebugger;
			
			if(isDebug){
				_starling.showStats = true;
				_starling.showStatsAt();
			}
			_starling.addEventListener(starling.events.Event.ROOT_CREATED , onRootCreated);

			// When the game becomes inactive, we pause Starling; otherwise, the enter frame event
			// would report a very long 'passedTime' when the app is reactivated. 

			_stage.addEventListener(flash.events.Event.ACTIVATE , function(e:*):void
			{
				_starling.start();
			});
			_stage.addEventListener(flash.events.Event.DEACTIVATE , function(e:*):void
			{
				_starling.stop();
			});
			
			return _assets;
		}

		public function loadExternalAssets(progressCallBack:Function):void
		{
			if(_assets)
				_assets.loadQueue(progressCallBack);
		}

		public function get onStarlingInitialized():Signal
		{
			return _onStarlingInitialized;
		}

		private function onRootCreated(event:Object , app:StarlingPortal):void
		{
			_starling.removeEventListener(starling.events.Event.ROOT_CREATED , onRootCreated);

			app.start();

			_starling.start();

			_onStarlingInitialized.dispatch(app);
			
			trace("================================= Starling initialized ==================================")
		}
	}
}