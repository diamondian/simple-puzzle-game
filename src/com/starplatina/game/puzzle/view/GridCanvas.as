//------------------------------------------------------------------------------
//
//   STAR PLATINA Copyright 2013 
//   Blandon Du All rights reserved. 
//
//------------------------------------------------------------------------------

package com.starplatina.game.puzzle.view
{
	import com.starplatina.game.puzzle.enum.AssetNames;
	import com.starplatina.game.puzzle.view.element.Gem;

	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.utils.AssetManager;

	public class GridCanvas extends Image
	{
		private var _assetManager:AssetManager;

		private var _gems:Vector.<Gem>;

		private var templates:Vector.<Image>

		private var _canvasTexture:RenderTexture;

		private var _isActive:Boolean;

		public function GridCanvas(assetManager:AssetManager, width:int, height:int)
		{
			_assetManager = assetManager;
			super(_canvasTexture = new RenderTexture(width, height, false));
			initializeTemplates();
			initializeClock();
		}

		public function get isActive():Boolean  { return _isActive; }

		public function set isActive(value:Boolean):void
		{
			_isActive = value;
		}

		private function initializeClock():void
		{
			this.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
		}

		private function onEnterFrame(event:EnterFrameEvent):void
		{
			_isActive && updateGridView();
		}

		private function initializeTemplates():void
		{
			templates = new Vector.<Image>();

			for (var i:int = 0; i < AssetNames.GEM_NAMES.length; i++)
			{
				var gem:Image = new Image(_assetManager.getTexture(AssetNames.GEM_NAMES[i]));
				//gem.smoothing = smoothing.;
				gem.pivotX = gem.pivotY = (gem.width >> 1);
				templates.push(gem);
			}
			var empty:Image = new Image(Texture.fromColor(gem.width, gem.height, 0x00000000));
			templates.push(empty);
		}

		public function set gems(value:Vector.<Gem>):void
		{
			_gems ||= value;
		}

		public function updateGridView():void
		{
			_canvasTexture.drawBundled(function():void
			{
				for each (var gem:Gem in _gems)
				{
					var template:Image = (gem.gemType >= 0) ? templates[gem.gemType] : templates[templates.length - 1]

					template.x = gem.x + template.pivotX;
					template.y = gem.y + template.pivotY;
					template.scaleX = template.scaleY = gem.scale;
					//matrix = new Matrix(gem.selected?1.1:1,0,0,gem.selected?1.1:1,gem.x,gem.y);		
					_canvasTexture.draw(template);
				}
			});
		}


	}
}
