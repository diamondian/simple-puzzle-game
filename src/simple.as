//##############################################################################
//
//  Copyright 2013 Blandon.Du   All rights reserved.  
//
//##############################################################################

package
{
	import com.starplatina.game.puzzle.core.StarlingInitializer;
	import com.starplatina.game.puzzle.core.StarlingPortal;
	import com.starplatina.game.puzzle.view.GameView;
	
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	
	import starling.utils.AssetManager;

	[SWF(width = 755, height = 600, frameRate = 60)]
	public class simple extends Sprite
	{
		private var _portal:StarlingPortal;
		private var _assetManager:AssetManager;

		public function simple()
		{
			var initializer:StarlingInitializer = new StarlingInitializer(this.stage);
			initializer.onStarlingInitialized.addOnce(onStarted);
			_assetManager = initializer.initialize(new Rectangle(0, 0, 755, 600), 755, 600,File.applicationDirectory.resolvePath("texture/atf"));
		}

		private function onStarted(app:StarlingPortal):void
		{
			_portal = app;
			_assetManager.loadQueue(function(p:Number):void{if (p == 1){start()}});
		}

		private function start():void
		{
			_portal.addChild(new GameView(_assetManager));
		}
	}
}