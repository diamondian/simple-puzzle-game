//------------------------------------------------------------------------------
//
//   STAR PLATINA Copyright 2013 
//   Blandon Du All rights reserved. 
//
//------------------------------------------------------------------------------

package com.starplatina.game.puzzle.view.element
{
	import com.greensock.TweenMax;

	import org.osflash.signals.Signal;

	public class Gem
	{
		private var SWAP_DURATION:Number = 0.3;
		private var CLEAR_DURATION:Number = 0.2;
		private var FALL_DURATION_UNIT:Number = 0.1;

		private var _hIndex:int;
		private var _vIndex:int;
		private var _gemKind:int;

		private var _gemLeft:Gem;
		private var _gemRight:Gem;
		private var _gemTop:Gem;
		private var _gemBottom:Gem;

		public var x:Number = 0;
		public var y:Number = 0;

		public var width:Number;
		public var height:Number;

		public var scale:Number = 0.9;


		private var _neighbors:Array;
		private var _horizontalNeighbors:Array;
		private var _verticalNeighbors:Array;


		private const _sigStateChanged:Signal = new Signal(Gem);
		private const _sigGemSwapped:Signal = new Signal(Gem);
		private const _sigGemCleared:Signal = new Signal(Gem);
		private const _sigGemLanded:Signal = new Signal(Gem);
		private const _sigGemRefresheed:Signal = new Signal(Gem);

		private var _tweenMove:TweenMax;

		private var _selected:Boolean;
		private var _swapping:Boolean;
		private var _gemKindToBeChanged:int = -1;
		private var _isRefreshing:Boolean;

		public function Gem()
		{
		}

		//============= initial ==============================================

		public function toString():String
		{
			return "(gem" + hIndex + "" + vIndex + ")";
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		public function get sigStateChanged():Signal{return _sigStateChanged;}

		public function get sigGemCleared():Signal{return _sigGemCleared;}
		
		public function get sigGemSwapped():Signal{return _sigGemSwapped;}

		public function get sigStateLanded():Signal{return _sigGemLanded;}

		public function get sigGemRefreshed():Signal{return _sigGemRefresheed;}

		public function get vIndex():int{return _vIndex;}
		public function set vIndex(value:int):void
		{
			_vIndex = value;
			y = value * height;
		}

		public function get hIndex():int{return _hIndex;}
		public function set hIndex(value:int):void
		{
			_hIndex = value;
			x = value * width;
		}

		public function get neighbors():Array{return _neighbors;}
		public function set neighbors(value:Array):void
		{
			_neighbors = value;

			_gemLeft = value[0];
			_gemRight = value[1];
			_gemTop = value[2];
			_gemBottom = value[3];

			//trace(this + _gemLeft + _gemRight + _gemTop + _gemBottom);
		}

		public function get gemBottom():Gem  { return _gemBottom; }

		public function get gemTop():Gem  { return _gemTop; }

		public function get gemRight():Gem  { return _gemRight; }

		public function get gemLeft():Gem  { return _gemLeft; }

		//======================================================================

		public function get gemType():int
		{
			return _gemKind;
		}

		public function set gemType(value:int):void
		{
			_gemKind = value;
			if (value < 0)
			{
				y = value * height; // put on top for dropping.
			}
		}


		//---------------- checker ---------------------------------------------------
		public function get isFormingChainVertically():Boolean
		{
			var top:Array = (_gemTop && (_gemTop.gemType == _gemKind)) ? _gemTop.upwardNeighbors : [];
			var bottom:Array = (_gemBottom && (_gemBottom.gemType == _gemKind)) ? _gemBottom.downwardNeighbors : [];
			_verticalNeighbors = top.concat(bottom).concat(this);

			return _verticalNeighbors.length > 2;
		}

		public function get isFormingChainHorizontally():Boolean
		{
			var left:Array = (_gemLeft && (_gemLeft.gemType == _gemKind)) ? _gemLeft.leftHandSideNeighbors : [];
			var right:Array = (_gemRight && (_gemRight.gemType == _gemKind)) ? _gemRight.rightHandSideNeighbors : [];
			_horizontalNeighbors = left.concat(right).concat(this);

			return _horizontalNeighbors.length > 2;
		}

		//---------------- relations ---------------------------------------------------
		public function get horizontalChain():Array
		{
			return _horizontalNeighbors;
		}

		public function get verticalChain():Array
		{
			return _verticalNeighbors;
		}

		//----------------------------
		public function get leftHandSideNeighbors():Array
		{
			return (_gemLeft && (_gemLeft.gemType == _gemKind)) ? _gemLeft.leftHandSideNeighbors.concat(this) : [this];
		}

		public function get rightHandSideNeighbors():Array
		{
			return (_gemRight && (_gemRight.gemType == _gemKind)) ? _gemRight.rightHandSideNeighbors.concat(this) : [this];
		}

		public function get upwardNeighbors():Array
		{
			return (_gemTop && (_gemTop.gemType == _gemKind)) ? _gemTop.upwardNeighbors.concat(this) : [this];
		}

		public function get downwardNeighbors():Array
		{
			return (_gemBottom && (_gemBottom.gemType == _gemKind)) ? _gemBottom.downwardNeighbors.concat(this) : [this];
		}

		//---------------------- actions ----------------------------------------------

		public function clear():void
		{
			scaleTo(0, CLEAR_DURATION, onCleared);
		}

		public function playSwapAnim(gem:Gem, doSwapGemKind:Boolean, swapPartner:Boolean = true):void
		{
			scaleTo(0.9, SWAP_DURATION);
			move(gem, SWAP_DURATION, !doSwapGemKind, onSwapped);

			if (doSwapGemKind)
			{
				_gemKindToBeChanged = gem.gemType;
			}

			swapPartner && gem.playSwapAnim(this, doSwapGemKind, false);
		}

		public function playFallAnim(targetGem:Gem, newGemKind:int):void
		{
			_gemKindToBeChanged = newGemKind;
			move(targetGem, FALL_DURATION_UNIT * (targetGem.vIndex - _vIndex), false, onLanded);
		}

		public function playRefreshAnim():void
		{
			scale = 0.9;
			move(this, FALL_DURATION_UNIT * (-y / height), false, onRefreshed);
		}

		public function set selected(value:Boolean):void
		{
			if (_selected == value)
				return;

			_selected = value;
			scale = value ? 1 : 0.9;
			_sigStateChanged.dispatch(this);
			//trace("Gem "+_hIndex + " " + _vIndex + "selected changed");
		}

		//====================== animation ==========================================
		private function move(gem:Gem, duration:Number, back:Boolean, callback:Function = null):void
		{
			var repeat:int = back ? 1 : 0;
			TweenMax.to(this, duration, {x: gem.hIndex * width, y: gem.vIndex * height, yoyo: back, repeat: repeat, onComplete: callback});
		}

		private function scaleTo(scaleFactor:Number, duration:Number, callback:Function = null):void
		{
			//trace("scale to "+scaleFactor);
			TweenMax.to(this, duration, {scale: scaleFactor, onComplete: callback});
			_selected = false;
		}

		private function onCleared():void
		{
			_sigGemCleared.dispatch(this);
		}

		private function onSwapped():void
		{
			//trace("swapped !");
			if (_gemKindToBeChanged != -1)
			{
				_gemKind = _gemKindToBeChanged;
				_gemKindToBeChanged = -1;
			}
			resetPosition();
			_sigGemSwapped.dispatch(this);
		}

		private function onLanded():void
		{
			resetPosition();
			gemType = _gemKindToBeChanged;
			_gemKindToBeChanged = -1;
			_sigGemLanded.dispatch(this);
		}

		private function onRefreshed():void
		{
			_sigGemRefresheed.dispatch(this);
		}

		private function resetPosition():void
		{
			y = _vIndex * height;
			x = _hIndex * width;
		}









	}
}