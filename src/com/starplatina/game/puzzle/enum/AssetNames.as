//------------------------------------------------------------------------------
//
//   STAR PLATINA Copyright 2013 
//   Blandon Du All rights reserved. 
//
//------------------------------------------------------------------------------

package com.starplatina.game.puzzle.enum
{

	public class AssetNames
	{
		static public const GEM_NAMES:Array = ["Red", "Yellow", "Purple", "Green", "Blue"];

		/*static public const GEM_Red:String = "Red";
		static public const GEM_Yellow:String = "Yellow";
		static public const GEM_Purple:String = "Purple";
		static public const GEM_Green:String = "Green";
		static public const GEM_Blue:String = "Blue";*/

		static public const BackGround:String = "BackGround";
	}
}
